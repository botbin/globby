package report

import (
	"context"

	proto "github.com/golang/protobuf/proto"
	kafka "github.com/segmentio/kafka-go"
)

// A Publisher emits build results.
type Publisher interface {
	Publish(message proto.Message) error
}

type kafkaPublisher struct {
	writer *kafka.Writer
}

// NewPublisher creates a Publisher that sends messages to a Kafka topic.
func NewPublisher(config PublisherConfig) Publisher {
	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers:      config.Brokers,
		Topic:        config.Topic,
		BatchSize:    config.BatchSize,
		BatchTimeout: config.BatchTimeout,
	})
	return &kafkaPublisher{writer: writer}
}

func (kp *kafkaPublisher) Publish(message proto.Message) error {
	body, err := proto.Marshal(message)
	if err != nil {
		return err
	}

	return kp.writer.WriteMessages(context.Background(), kafka.Message{
		// TODO: Consider if using a key would make sense here.
		Value: body,
	})
}
