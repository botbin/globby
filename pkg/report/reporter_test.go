package report_test

import (
	"testing"
	"time"

	"gitlab.com/botbin/builder/pkg/report"
	"gitlab.com/botbin/builder/pkg/build/serial"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/builder/pkg/report/mocks"
)

type ReporterTestSuite struct {
	suite.Suite
}

// Ensures that reporting a success results in a published Success result.
func (rs *ReporterTestSuite) TestReportSuccess() {
	incomplete := new(mocks.Publisher)
	complete := new(mocks.Publisher)
	complete.On("Publish", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		result, ok := args[0].(*report.Result)
		rs.True(ok)
		rs.NotNil(result.GetSuccess())
	})

	reporter := report.NewReporter(complete, incomplete)
	err := reporter.ReportSuccess(new(serial.Request), time.Second, new(serial.Location))
	rs.Nil(err)

	incomplete.AssertNotCalled(rs.T(), "Publish")
	complete.AssertExpectations(rs.T())
}

// Ensures that reporting a failure results in a published Failure result.
func (rs *ReporterTestSuite) TestReportFailure() {
	incomplete := new(mocks.Publisher)
	complete := new(mocks.Publisher)
	complete.On("Publish", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		result, ok := args[0].(*report.Result)
		rs.True(ok)
		rs.NotNil(result.GetFailure())
	})

	reporter := report.NewReporter(complete, incomplete)
	err := reporter.ReportFailure(new(serial.Request), time.Second, "test")
	rs.Nil(err)

	incomplete.AssertNotCalled(rs.T(), "Publish")
	complete.AssertExpectations(rs.T())
}

// Ensures that reporting an incomplete results in a published request.
func (rs *ReporterTestSuite) TestReportIncomplete() {
	complete := new(mocks.Publisher)
	incomplete := new(mocks.Publisher)
	request := new(serial.Request)
	incomplete.On("Publish", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		req, ok := args[0].(*serial.Request)
		rs.Equal(request, req)
		rs.True(ok)
		rs.NotNil(req)
	})

	reporter := report.NewReporter(complete, incomplete)
	err := reporter.ReportIncomplete(request)
	rs.Nil(err)

	complete.AssertNotCalled(rs.T(), "Publish")
	incomplete.AssertExpectations(rs.T())
}

func TestReporter(t *testing.T) {
	suite.Run(t, new(ReporterTestSuite))
}
