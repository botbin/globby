package report

import (
	"time"

	"gitlab.com/botbin/builder/pkg/build/serial"
)

// A Reporter reports the results of building modules.
type Reporter interface {
	ReportSuccess(*serial.Request, time.Duration, *serial.Location) error
	ReportFailure(r *serial.Request, d time.Duration, reason string) error
	ReportIncomplete(*serial.Request) error
}

// protoReporter uses build results to create protobuf reports that are
// sent to publishers.
type protoReporter struct {
	complete   Publisher
	incomplete Publisher
}

// NewReporter creates a reporter that publishes two kinds of reports.
//
// The incomplete publisher is used to report builds that were unable to
// finish do to system error. The corresponding method is ReportIncomplete.
// The complete publisher is used for all other reports.
func NewReporter(complete, incomplete Publisher) Reporter {
	return &protoReporter{
		complete:   complete,
		incomplete: incomplete,
	}
}

// ReportSuccess reports that a module build completed successfully.
func (pr *protoReporter) ReportSuccess(req *serial.Request, dur time.Duration,
	loc *serial.Location) error {
	return pr.complete.Publish(&Result{
		Definition:      req.GetDefinition(),
		BuildDurationMs: int32(dur.Seconds() / 1000),
		Payload: &Result_Success{
			Success: &Success{
				Location: loc,
			},
		},
	})
}

// ReportFailure reports that a module build completed but failed.
func (pr *protoReporter) ReportFailure(req *serial.Request, dur time.Duration,
	reason string) error {
	return pr.complete.Publish(&Result{
		Definition:      req.GetDefinition(),
		BuildDurationMs: int32(dur.Seconds() / 1000),
		Payload: &Result_Failure{
			Failure: &Failure{
				Reason: reason,
			},
		},
	})
}

// ReportIncompleted reports that a module build was unable to finish.
func (pr *protoReporter) ReportIncomplete(req *serial.Request) error {
	return pr.incomplete.Publish(req)
}
