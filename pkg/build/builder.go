package build

import (
	"os"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/botbin/builder/pkg/build/function"
	"gitlab.com/botbin/builder/pkg/build/scaffold"
	"gitlab.com/botbin/builder/pkg/build/serial"
)

// A Builder uses a request to create the function for a module.
type Builder interface {
	Build(*serial.Definition) (*serial.Location, error)
}

// ofModuleBuilder handles creation of OpenFaaS modules.
// Objects of this type are safe for concurrent use.
type ofModuleBuilder struct {
	wrkDir     string
	asmFactory scaffold.AssemblerFactory
}

// New creates a Builder instance that operates within a given directory.
func New(wrkDir string) Builder {
	return &ofModuleBuilder{
		wrkDir:     wrkDir,
		asmFactory: scaffold.NewOFAssemblerFactory(),
	}
}

// Build creates an OpenFaaS module image.
func (imb *ofModuleBuilder) Build(def *serial.Definition) (*serial.Location, error) {
	dir, err := imb.createWorkingDirectory()
	if err != nil {
		return nil, err
	}

	if ref, err := imb.assignWorkerTask(def, dir); err != nil {
		imb.removeWorkingDirectory(dir)
		return nil, err
	} else {
		return ref.Location, imb.removeWorkingDirectory(dir)
	}
}

// createWorkingDirectory creates a temporary directory to build the module in.
func (imb *ofModuleBuilder) createWorkingDirectory() (string, error) {
	dir := imb.wrkDir + "/" + uuid.NewV4().String()
	err := os.Mkdir(dir, 0700)
	return dir, err
}

// assignWorkerTask instructs a worker to build the module.
func (imb *ofModuleBuilder) assignWorkerTask(def *serial.Definition, workDir string) (function.FRef, error) {
	ref := function.NewRef(def.GetMeta(), workDir)
	worker := function.NewWorker(def.GetSpec(), ref, imb.asmFactory)
	return ref, worker.Make()
}

// removeWorkingDirectory removes the temporary directory where the module was built.
func (imb *ofModuleBuilder) removeWorkingDirectory(dir string) error {
	return os.RemoveAll(dir)
}
