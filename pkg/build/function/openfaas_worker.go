package function

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/botbin/builder/pkg/build/scaffold"
	"gitlab.com/botbin/builder/pkg/build/serial"
	"go.uber.org/zap"
)

// A Worker is responsible for creating and publishing a module function.
type Worker interface {
	Make() error
}

// ofWorker creates a single OpenFaaS-based module.
// These objects should not be reused.
type ofWorker struct {
	spec    *serial.Spec
	fn      FRef
	factory scaffold.AssemblerFactory
}

// NewWorker creates a worker that builds functions for OpenFaaS.
func NewWorker(spec *serial.Spec, fn FRef, factory scaffold.AssemblerFactory) Worker {
	return ofWorker{
		spec:    spec,
		fn:      fn,
		factory: factory,
	}
}

// Make creates and deploys an OpenFaaS module.
func (iw ofWorker) Make() error {
	if err := iw.generateScaffold(); err != nil {
		return UserError{Reason: err.Error()}
	}
	if err := iw.createFunctionFile(); err != nil {
		return SystemError{Reason: err.Error()}
	}
	return iw.publishModule()
}

// run executes a command in the working directory, logging output only
// if the command fails. The command is given to /bin/bash.
func (iw ofWorker) run(cmd string) error {
	op := exec.Command("/bin/bash", "-c", cmd)
	op.Dir = iw.fn.BuildDir
	output, err := op.Output()

	if err != nil {
		zap.S().Warnw("external command failure",
			"command", strings.Join(op.Args, " "),
			"output", string(output),
		)
	}
	return err
}

// generateScaffold generates template OpenFaaS files that will be modified
// to include module code.
func (iw ofWorker) generateScaffold() error {
	lang := scaffold.GetLanguageCode(iw.spec.GetLanguage())
	input := fmt.Sprintf(`faas-cli new %s --lang %s`, iw.fn.ServiceName, lang)
	if err := iw.run(input); err != nil {
		return UserError{Reason: err.Error()}
	}

	return newOfConfigUpdater(iw.fn).Update()
}

// createFunctionFile creates the final OpenFaaS files that will be deployed.
func (iw ofWorker) createFunctionFile() error {
	extension := scaffold.GetLanguageExtension(iw.spec.GetLanguage())
	filename := iw.fn.ServiceName + "/handler." + extension

	f, err := os.Create(iw.fn.BuildDir + "/" + filename)
	if err != nil {
		return err
	}
	defer f.Close()

	return iw.writeModule(f)
}

// writeModule writes the user's module contents into an OpenFaaS function.
func (iw ofWorker) writeModule(f *os.File) error {
	assembler, err := iw.factory.Get(iw.spec.GetLanguage())
	if err != nil {
		return err
	}

	contents, err := assembler.Assemble(iw.spec.GetCode())
	if err != nil {
		return err
	}

	_, err = f.WriteString(contents)
	return err
}

// publishModule deploys a user's module to the production function service.
func (iw ofWorker) publishModule() error {
	build := fmt.Sprintf("faas-cli build -f %s.yml", iw.fn.ServiceName)
	if err := iw.run(build); err != nil {
		return UserError{Reason: err.Error()}
	}

	push := fmt.Sprintf("faas-cli push -f %s.yml", iw.fn.ServiceName)
	if err := iw.run(push); err != nil {
		return SystemError{Reason: err.Error()}
	}

	deploy := fmt.Sprintf("faas-cli deploy -f %s.yml", iw.fn.ServiceName)
	if err := iw.run(deploy); err != nil {
		return SystemError{Reason: err.Error()}
	}
	return nil
}
