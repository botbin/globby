package function

import (
	"os"
	"sync"
)

var (
	regOnce        sync.Once
	moduleRegistry string
)

// getModuleRegistry gets the registry URL for module images.
// This method will panic if the environment variable is missing.
func getModuleRegistry() string {
	regOnce.Do(func() {
		moduleRegistry = os.Getenv("MODULE_REGISTRY")
		if moduleRegistry == "" {
			panic("missing var MODULE_REGISTRY")
		}
	})
	return moduleRegistry
}

var (
	regPSOnce     sync.Once
	regPullSecret string
)

// getRegistryPullSecret gets name of the k8s registry pull secret.
// This method will panic if the environment variable is missing.
func getRegistryPullSecret() string {
	regPSOnce.Do(func() {
		regPullSecret = os.Getenv("REGISTRY_PULL_SECRET")
		if regPullSecret == "" {
			panic("missing var REGISTRY_PULL_SECRET")
		}
	})
	return regPullSecret
}

var (
	gatewayOnce     sync.Once
	functionGateway string
)

// getFunctionGateway gets the URL to the OpenFaaS gateway.
// This method will panic if the environment variable is missing.
func getFunctionGateway() string {
	gatewayOnce.Do(func() {
		functionGateway = os.Getenv("OPEN_FAAS_GATEWAY")
		if functionGateway == "" {
			panic("missing var OPEN_FAAS_GATEWAY")
		}
		functionGateway = `http://` + functionGateway
	})
	return functionGateway
}
