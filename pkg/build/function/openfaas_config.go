package function

import (
	"fmt"
	"io/ioutil"
	"regexp"
)

// ofConfigUpdater modifies the config file for an OpenFaaS function.
type ofConfigUpdater struct {
	fn     FRef
	config string
}

// Update triggers the modification of the config file.
func (oc *ofConfigUpdater) Update() error {
	oc.updateImageURL()
	oc.updateGatewayURL()
	return oc.writeFile()
}

func (oc *ofConfigUpdater) updateImageURL() {
	re := regexp.MustCompile(`(image:) (.*)`)
	// The formatting here matters since we insert into a yaml file.
	replacement := fmt.Sprintf(
		`$1 %s
    secrets:
      - %s`,
		oc.fn.Location.GetDockerImageUrl(), getRegistryPullSecret(),
	)

	oc.config = re.ReplaceAllString(oc.config, replacement)
}

func (oc *ofConfigUpdater) updateGatewayURL() {
	re := regexp.MustCompile(`(gateway:) (.*)`)
	replacement := fmt.Sprintf("$1 %s", getFunctionGateway())
	oc.config = re.ReplaceAllString(oc.config, replacement)
}

func (oc *ofConfigUpdater) writeFile() error {
	return ioutil.WriteFile(oc.fn.BuildDir+"/"+oc.fn.ServiceName+".yml", []byte(oc.config), 0644)
}

func newOfConfigUpdater(fn FRef) *ofConfigUpdater {
	configContents, _ := ioutil.ReadFile(fn.BuildDir + "/" + fn.ServiceName + ".yml")
	return &ofConfigUpdater{
		fn:     fn,
		config: string(configContents),
	}
}
