package function

import (
	"strings"

	"gitlab.com/botbin/builder/pkg/build/serial"
)

// FRef details a OpenFaaS function to be constructed.
type FRef struct {
	// BuildDir is the directory where the function is being built.
	BuildDir string
	// ServiceName is the name given to the function service.
	ServiceName string
	// The location of the function in the OpenFaaS cluster
	Location *serial.Location
}

func NewRef(meta *serial.Metadata, buildDir string) FRef {
	serviceName := meta.GetName() + "-" + strings.Replace(meta.GetTag(), ".", "-", -1)

	return FRef{
		BuildDir: buildDir,
		Location: &serial.Location{
			DockerImageUrl: getModuleRegistry() + "/" + meta.GetFqn(),
			FunctionUrl:    "http://" + getFunctionGateway() + "/function/" + serviceName,
		},
		ServiceName: serviceName,
	}
}
