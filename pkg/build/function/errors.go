package function

// UserError indicates that a build failed due to a user's
// misunderstanding of how modules should be created.
type UserError struct {
	Reason string
}

func (ue UserError) Error() string {
	return "user-supplied module definition is invalid; reason: " + ue.Reason
}

// SystemError indicates that a build failed due to the failure
// of an external process.
type SystemError struct {
	Reason string
}

func (se SystemError) Error() string {
	return "module build failed due to system instability; reason: " + se.Reason
}