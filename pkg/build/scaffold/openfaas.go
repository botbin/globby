package scaffold

import (
	"errors"
	"fmt"
)

// ofAssemblerFactory creates a ModuleAssembler for OpenFaaS.
type ofAssemblerFactory struct {
}

// Get retrieves a ModuleAssembler that produces code for a given language.
func (iaf ofAssemblerFactory) Get(language string) (ModuleAssembler, error) {
	if language == JavaScript {
		return ofJSAssembler{}, nil
	}
	return nil, errors.New("language not supported")
}

// NewOFAssemblerFactory creates an AssemblerFactory for OpenFaaS.
func NewOFAssemblerFactory() AssemblerFactory {
	return ofAssemblerFactory{}
}

// ofJSAssembler assembles OpenFaaS functions for JavaScript.
type ofJSAssembler struct {
}

// Assemble generates a function handler that will call a user's code.
func (iga ofJSAssembler) Assemble(userCode string) (string, error) {
	return fmt.Sprintf(`
"use strict"

// function accept(payload) { /* return string */ }
%s

module.exports = (context, callback) => {
	callback(undefined, accept(context));
}		
`, userCode), nil
}
