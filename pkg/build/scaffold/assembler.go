package scaffold

// ModuleAssembler turns user source code into a callable module.
type ModuleAssembler interface {
	Assemble(userSourceCode string) (completeCode string, err error)
}

// AssemblerFactory creates a module assembler for a specific language.
type AssemblerFactory interface {
	Get(language string) (ModuleAssembler, error)
}
