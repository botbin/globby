package scaffold

const (
	// JavaScript is the key for the JavaScript langauge.
	JavaScript string = "js"
)

// GetLanguageExtension gets the file extension for a language.
func GetLanguageExtension(lang string) string {
	if lang == JavaScript {
		return "js"
	}
	return ""
}

// GetLanguageCode gets the OpenFaaS code for a language.
func GetLanguageCode(lang string) string {
	if lang == JavaScript {
		return "node"
	}
	return ""
}
