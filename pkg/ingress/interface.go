package ingress

import (
	"time"

	"gitlab.com/botbin/builder/pkg/build/serial"
)

// An Ingress retrieves batches of requests from an external source.
type Ingress interface {
	Listen()
	Close() error
	Requests() <-chan []*serial.Request
	Commit() error
}

// Config specifies how an Ingress should function.
type Config struct {
	// Brokers is the list of broker addresses to connect to.
	Brokers []string

	// Topic specifies where requests will come from.
	Topic string

	// BatchSize is the maximum number of requests to fetch at a time.
	// The actual batch size may be less if the timeout is reached.
	BatchSize int

	// BatchTimeout is the maximum time to wait in order to accumulate
	// a full request batch.
	BatchTimeout time.Duration

	// The minimum time to wait in order to process a request after
	// it has been committed.
	ReadDelay time.Duration
}
