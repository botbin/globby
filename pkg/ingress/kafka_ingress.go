package ingress

import (
	"context"
	"time"

	"github.com/golang/protobuf/proto"
	kafka "github.com/segmentio/kafka-go"
	"gitlab.com/botbin/builder/pkg/build/serial"
	"go.uber.org/zap"
)

type kafkaIngress struct {
	config   Config
	requests chan []*serial.Request

	reader    *kafka.Reader
	msgs      []kafka.Message
	committed chan bool

	listening bool
}

// New creates an Ingress that gets its requests from Kafka.
func New(config Config) Ingress {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:       config.Brokers,
		Topic:         config.Topic,
		GroupID:       "builder",
		QueueCapacity: config.BatchSize,
	})

	return &kafkaIngress{
		config:    config,
		reader:    reader,
		requests:  make(chan []*serial.Request),
		committed: make(chan bool),
	}
}

func (ki *kafkaIngress) Listen() {
	for {
		deadline := time.Now().Add(ki.config.BatchTimeout)
		ctx, cancel := context.WithDeadline(context.TODO(), deadline)

		go ki.loadBatch(ctx, cancel)
		<-ctx.Done()
		if len(ki.msgs) == 0 {
			continue
		}

		requests, whenReady := ki.convertBatch()
		ki.requests <- requests
		<-ki.committed // Wait for ki.Commit() to be called.

		now := time.Now().UTC()
		if whenReady.After(now) {
			time.Sleep(whenReady.Sub(now))
		}
	}
}

func (ki *kafkaIngress) Close() error {
	return ki.reader.Close()
}

// loadBatch attempts to load a full batch of messages before the context deadline.
func (ki *kafkaIngress) loadBatch(ctx context.Context, finish func()) {
	for i := 0; i < ki.config.BatchSize; i++ {
		m, err := ki.reader.FetchMessage(ctx)
		if err != nil {
			break
		}
		ki.msgs = append(ki.msgs, m)
	}
	finish()
}

// convertBatch converts the Kafka messages into protobuf messages.
//
// It will only convert messages if the current time is later than their
// timestamp plus the read delay. Messages after the first unready
// message are postponed until the next batch.
//
// The time returned is the point when the next postponed message is ready.
// If <= time.Now().UTC(), the entire batch was converted successfully.
func (ki *kafkaIngress) convertBatch() ([]*serial.Request, time.Time) {
	var requests []*serial.Request
	now := time.Now().UTC()

	for i := range ki.msgs {
		if ready, whenReady := ki.isReady(now, i); !ready {
			ki.reader.SetOffset(ki.msgs[i].Offset)
			ki.msgs = ki.msgs[:i]
			return requests, whenReady
		}

		request := new(serial.Request)
		err := proto.Unmarshal(ki.msgs[i].Value, request)
		if err != nil {
			zap.S().Errorw("failed to parse request", "error", err, "message", ki.msgs[i])
			continue
		}

		requests = append(requests, request)
	}
	return requests, now
}

func (ki *kafkaIngress) isReady(now time.Time, msgIdx int) (bool, time.Time) {
	if ki.config.ReadDelay == 0 {
		return true, now
	}

	notBefore := ki.msgs[msgIdx].Time.UTC().Add(ki.config.ReadDelay)
	ready := now.After(notBefore)
	return ready, notBefore
}

func (ki *kafkaIngress) Requests() <-chan []*serial.Request {
	return ki.requests
}

func (ki *kafkaIngress) Commit() error {
	defer func() { ki.committed <- true }()

	err := ki.reader.CommitMessages(context.Background(), ki.msgs...)
	ki.msgs = nil
	return err
}
