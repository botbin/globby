package super

import (
	"sync"
	"time"

	"gitlab.com/botbin/builder/pkg/build"
	"gitlab.com/botbin/builder/pkg/build/function"
	"gitlab.com/botbin/builder/pkg/ingress"
	"gitlab.com/botbin/builder/pkg/report"
	"gitlab.com/botbin/builder/pkg/build/serial"
	"go.uber.org/zap"
)

// Supervisor supervises the ingress-build-report process.
type Supervisor struct {
	in       ingress.Ingress
	builder  build.Builder
	reporter report.Reporter
}

// New creates a Supervisor instance but does not start it.
//
// The provided ingress should be listening before passing it to
// this method. Failure to do so will result in a deadlock.
func New(in ingress.Ingress, builder build.Builder, reporter report.Reporter) *Supervisor {
	return &Supervisor{
		in:       in,
		builder:  builder,
		reporter: reporter,
	}
}

// Start starts a blocking supervision.
func (s *Supervisor) Start() {
	for requests := range s.in.Requests() {
		s.buildAll(requests)
		if err := s.in.Commit(); err != nil {
			zap.S().Errorw("failed to commit requests", "error", err)
		}
	}
}

func (s *Supervisor) buildAll(requests []*serial.Request) {
	var wg sync.WaitGroup
	wg.Add(len(requests))

	for _, request := range requests {
		go s.build(request, &wg)
	}
	wg.Wait()
}

func (s *Supervisor) build(request *serial.Request, wg *sync.WaitGroup) {
	defer wg.Done()

	start := time.Now()
	location, err := s.builder.Build(request.GetDefinition())
	duration := time.Now().Sub(start)

	if err == nil {
		err = s.reporter.ReportSuccess(request, duration, location)
	} else if e, ok := err.(function.UserError); ok {
		err = s.reporter.ReportFailure(request, duration, e.Error())
	}

	if err != nil {
		fqn := request.GetDefinition().GetMeta().GetFqn()
		zap.S().Errorw("reporting incomplete request", "module.fqn", fqn, "error", err)
		s.reporter.ReportIncomplete(request)
	}
}
