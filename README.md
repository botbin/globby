# builder
builder transforms module definitions into invocable functions.

## Build Process
Build requests are consumed from a Kafka topic. The specs they contain are
used to create functions in an [OpenFaaS](https://github.com/openfaas/faas) cluster.
Any build will have one of three outcomes:
1. The build succeeds, in which case the result is published to the
   `module.build.result` topic.
2. The build fails due to an error in the user's module definition. This
   result also gets sent to the `module.build.result` topic.
3. The build fails due to the failure of an external service. That service
   could be the OpenFaaS gateway or the Docker registry where function
   images are stored. In this case, the result percolates through a sequence
   of retry topics that eventually lead to a dead letter queue.

## Schemas
This service uses [protocol buffers](https://github.com/google/protobuf) as
the output format to Kafka. Any schema that it owns can be found in the
[schemas directory](./schema).

## Environment
### Working Directories
builder relies on CLI tooling from OpenFaaS to build functions. This means
a scratch disk is necessary for intermediate Docker steps. The provided
[Dockerfile](./docker/Dockerfile) defines the standard volume mount path.

### Variables
- OPEN_FAAS_GATEWAY
    - The address for the OpenFaaS gateway
- MODULE_REGISTRY
    - The Docker registry URL for function images
- REGISTRY_PULL_SECRET
    - The name of the Kubernetes image pull secret that grants access
      to the module registry
- BUILDER_WRKDIR
    - The directory where scratch builds take place
- KAFKA_BROKERS
    - A comma-separated list of addresses of Kafka brokers
- BATCH_SIZE
    - The number of modules to build concurrently
- REQUEST_TOPIC
    - The Kafka topic where requests are consumed
- RETRY_TOPIC
    - The Kafka topic where requests are submitted to be reprocessed
- RETRY_DELAY
    - This value is relevant to instances whose REQUEST_TOPIC value is itself a
      retry topic. It is the duration used to perform a Not Before check
      in order to process each request, where NBF = Commit Time + RETRY_DELAY.
      This should not be set if REQUEST_TOPIC is the main request topic.
      Values may be standard duration formats, such as 5s, 10m, or 1h.