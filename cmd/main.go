package main

import (
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/botbin/builder/pkg/build"
	"gitlab.com/botbin/builder/pkg/report"
	"gitlab.com/botbin/builder/pkg/super"

	"gitlab.com/botbin/builder/pkg/ingress"
	"go.uber.org/zap"
)

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}

const version = "3.0.0"

func main() {
	zap.S().Infof("started builder v%s", version)

	batchSize, err := strconv.Atoi(os.Getenv("BATCH_SIZE"))
	if err != nil || batchSize < 1 {
		zap.S().Panicw("invalid value for variable BATCH_SIZE")
	}
	brokers := strings.Split(os.Getenv("KAFKA_BROKERS"), ",")

	in := getIngress(brokers, batchSize)
	reporter := getReporter(brokers, batchSize)
	builder := build.New(os.Getenv("BUILDER_WRKDIR"))
	supervisor := super.New(in, builder, reporter)

	wireGracefulExit(in)
	go in.Listen()
	supervisor.Start()
}

func getIngress(brokers []string, batchSize int) ingress.Ingress {
	config := ingress.Config{
		Brokers:      brokers,
		Topic:        os.Getenv("REQUEST_TOPIC"),
		BatchSize:    batchSize,
		BatchTimeout: time.Second,
	}

	if delay := os.Getenv("RETRY_DELAY"); delay != "" {
		var err error
		config.ReadDelay, err = time.ParseDuration(delay)
		if err != nil {
			zap.S().Panicw("invalid value for RETRY_DELAY")
		}
	}
	return ingress.New(config)
}

func getReporter(brokers []string, batchSize int) report.Reporter {
	complete := report.NewPublisher(report.PublisherConfig{
		Brokers:      brokers,
		Topic:        "module.build.result",
		BatchSize:    batchSize,
		BatchTimeout: time.Second,
	})

	incomplete := report.NewPublisher(report.PublisherConfig{
		Brokers:      brokers,
		Topic:        os.Getenv("RETRY_TOPIC"),
		BatchSize:    batchSize,
		BatchTimeout: time.Second,
	})
	return report.NewReporter(complete, incomplete)
}

// wireGracefulExit ensures that communication ends before shutting down.
func wireGracefulExit(in ingress.Ingress) {
	exit := make(chan os.Signal)
	signal.Notify(exit, syscall.SIGTERM)
	signal.Notify(exit, syscall.SIGINT)

	go func() {
		<-exit
		zap.S().Infow("accepted shutdown signal...")

		zap.S().Infow("stopping ingress...")
		in.Close()
		zap.S().Infow("ingress stopped")

		// TODO: Try to quickly clean up the current build process.

		zap.S().Infow("exiting")
		os.Exit(0)
	}()
}
